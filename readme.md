# Bootstrap a K3S cluster on Proxmox using Terraform

uses cloud-init to create blazing fast k3s clusters from templates

## Prerequesits 

a base template with k3s installed using INSTALL_K3S_SKIP_START=true

## compatibility

this was tested against:

 - Proxmox 7.0-10
 - Terraform v1.0.2
 - telmate/proxmox 2.7.4
 - k3s v1.21.2
 - ubuntu 20.10 cloudimage

## Quickstart 

## Links

* [Proxmox](https://proxmox.com)
* [Proxmox Terraform Provider](https://github.com/Telmate/terraform-provider-proxmox)
* [K3S](https://k3s.io)