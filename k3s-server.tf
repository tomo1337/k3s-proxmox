resource "proxmox_vm_qemu" "k3s-server" {

    name = "k3s-server"
    target_node = "fat"
    pool = "k8s"
    clone = "k3s" # source template

    # [CPU]
    sockets = 1
    cores = 4
    vcpus = 4

    # [MEMORY]
    memory = 2048
    balloon = 0

    # [STORAGE]
    scsihw = "virtio-scsi-single"
      disk {
        slot = 0
        type = "scsi"
        size = "4G"
        storage = "ssd"
        iothread = 1
        ssd = 1
      }

    # [NETWORK]
    #ipconfig0 = "ip=10.90.90.10/24,gw=10.90.90.1"
    ipconfig0 = "ip=dhcp"
    searchdomain = "vms.lan"
    network {
        model = "virtio"
        bridge = "vmbr0"
    }

    # [SSH]  
    sshkeys = file("~/.ssh/id_rsa.pub")
    ssh_private_key = file("~/.ssh/id_rsa")

    # [BOOTSTRAP K3S]
    provisioner "file" {
      source      = "manifests"
      destination = "/home/ubuntu/manifests"
    }
    provisioner "remote-exec" {
      inline = [
        "sudo mkdir -p /var/lib/rancher/k3s/server/manifests",
        "sudo mv /home/ubuntu/manifests /var/lib/rancher/k3s/server/manifests"
      ]
    }    
}