terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.7.4"
    }
  }
}

provider "proxmox" {
    pm_tls_insecure = true
    pm_api_url = ""
    pm_password = ""
    pm_user = ""
}

